import { Icon, Nav, Navbar, NavbarProps } from "rsuite";

export const HeaderNav = ({ appearance = 'inverse' } : NavbarProps) => {
  return (
    <Navbar appearance={appearance}>
      <Navbar.Header>
        <a href="./" className="navbar-brand logo">
          WELCOME
        </a>
      </Navbar.Header>
      <Navbar.Body>
        <Nav>
          <Nav.Item eventKey="1" icon={<Icon icon="home" />}>
            Home
          </Nav.Item>
        </Nav>
      </Navbar.Body>
    </Navbar>
  );
};

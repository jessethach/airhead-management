import { FlexboxGrid } from 'rsuite';

type AirporLabelProps = {
    name: string;
    time: string;
}

export const AirportLabel = ({ name, time }: AirporLabelProps) => {
    return (
        <FlexboxGrid>
            <FlexboxGrid.Item 
                style={{
                    flexDirection: 'column',
                    alignItems: 'flex-start',
                    overflow: 'hidden'
                }}
            >
                <p>{name}</p>
                <p>{time}</p>
            </FlexboxGrid.Item>
        </FlexboxGrid>
    )
}

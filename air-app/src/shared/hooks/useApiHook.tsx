import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
import { useCallback } from 'react';
import * as qs from 'qs';

const axiosInstance: AxiosInstance = axios.create({
    paramsSerializer: params => qs.stringify(params, { arrayFormat: 'repeat' }),
});

type CancelRequest = {
    requestCanceled: boolean
}

type CallbackType = <T>(
    path: string,
    extraConfig?: AxiosRequestConfig
) => Promise<(AxiosResponse<T> | CancelRequest)>;

export const isCanceled = (
    resp: AxiosResponse<unknown> | CancelRequest
): resp is CancelRequest =>
(resp as CancelRequest).requestCanceled !== undefined;

export const useApiHook = () => {
    const throwError = (error: any) => {
        if (axios.isCancel(error)) {
            return {
                requestCanceled: true
            }
        } else {
            if (error.response) {
                throw error.response.data;
            } else if (error.request) {
                throw error.request;
            } else if (error.message) {
                throw error.message;
            } else {
                throw error;
            }
        }
    }

    const getFetch = useCallback<CallbackType>((
        path: string, extraConfig: AxiosRequestConfig = {}
    ) => {
        return axiosInstance
            .get(`${path}`, extraConfig)
            .then(response => {
                if (response?.data) {
                    return response;
                } else {
                    throw new Error('No data returned from call');
                }
            }).catch(throwError);
    }, []);

    return {
        getFetch
    }
}

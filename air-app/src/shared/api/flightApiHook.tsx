import { useCallback } from 'react';
import { FlightApi } from '../constants/constants';
import { useApiHook, isCanceled } from '../hooks/useApiHook';
import { Flight } from '../models/flight';
import { GenericResponse } from '../models/genericResponse';
import { AxiosResponse } from 'axios';

type FlightResponse = AxiosResponse<GenericResponse<Flight[]>>;

export const useFlightApiHook = () => {
    const { getFetch } = useApiHook();

    const fetchFlights = useCallback(async () => {
        try {
            const response = await getFetch<FlightResponse>(`${FlightApi}`);

            if (!isCanceled(response)) {
                return response.data;
            }
        } catch (error) {
            console.error(error);

            return error;
        }
    }, [getFetch]);

    return { 
        fetchFlights
     };
}

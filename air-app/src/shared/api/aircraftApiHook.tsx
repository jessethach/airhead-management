import { useCallback } from 'react';
import { AircraftApi } from '../constants/constants';
import { useApiHook, isCanceled } from '../hooks/useApiHook';
import { AirCraft } from '../models/aircraft';
import { GenericResponse } from '../models/genericResponse';
import { AxiosResponse } from 'axios';

type AircraftResponse = AxiosResponse<GenericResponse<AirCraft[]>>;

export const useAircraftApiHook = () => {
    const { getFetch } = useApiHook();

    const fetchAircrafts = useCallback(async () => {
        try {
            const response = await getFetch<AircraftResponse>(`${AircraftApi}`);
            
            if (!isCanceled(response)) {
                return response.data;
            }
        } catch (error) {
            console.error(error);

            return error;
        }
    }, [getFetch]);

    return {
        fetchAircrafts
    };
}

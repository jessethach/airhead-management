export type ResponsePagination = {
    offset: number;
    limit: number;
    total: number;
}

export type GenericResponse<T> = {
    pagination: ResponsePagination;
    data: T;
}

export type AirCraft = {
    ident: string;
    type: string;
    economySeats: number;
    base: string;
}

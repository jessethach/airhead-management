export type Payload = {
  collection: string | number;
  node: HTMLElement;
  newIndex: number;
  oldIndex: number;
}

export type PayloadCallback = {
  (payload?: Payload, event?: MouseEvent): any;
}

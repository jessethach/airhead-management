import { AircraftList } from './components/aircraft-list/aircraftList';
import { Content, Divider, FlexboxGrid, Footer } from 'rsuite';
import { HeaderNav } from './shared/header-nav/headerNav';
import { FlightList } from './components/flight-list/flightList';
import { RotationList } from './components/rotation-list/rotationList';
import { useCallback, useState } from 'react';
import { Flight } from './shared/models/flight';

import './App.css';
import 'rsuite/dist/styles/rsuite-default.css';
import { Payload } from './shared/types/payloadCallback';

export const App = () => {
  const [selectedFlights, setSelectedFlights] = useState<Flight[]>([]);

  const onSelectFlight = useCallback((flight: Flight) => {
    setSelectedFlights(prev => [...prev, flight]);
  }, [setSelectedFlights]);

  const onRemoveFlight = useCallback((index: number) => {
    setSelectedFlights(prev => prev.filter((_f, i) => i !== index));
  }, [setSelectedFlights]);

  const onSortSelectedFlights = useCallback((payload?: Payload ) => {
    if (payload) {
      const { oldIndex, newIndex } = payload;

      setSelectedFlights(prev => {
        const moveData = prev.splice(oldIndex, 1);
        const newData = [...prev];
        newData.splice(newIndex, 0, moveData[0]);
        return newData;
      });
    }
  }, [setSelectedFlights]);

  return (
    <div className="App">
      <HeaderNav />
      <Content>
        <Divider />
        <FlexboxGrid justify="space-around">
          <FlexboxGrid.Item colspan={5} md={6}>
            <section className="flex-column">
              <div className="title-area">
                <p>Aircrafts</p>
              </div>
              <AircraftList />  
            </section>
          </FlexboxGrid.Item>
          <FlexboxGrid.Item colspan={9} md={6}>
            <section className="flex-column">
              <div className="title-area">
                <p>Rotation</p>
              </div>
              <RotationList selectedFlights={selectedFlights} selectFlight={onSelectFlight}/>
            </section>
          </FlexboxGrid.Item>
          <FlexboxGrid.Item colspan={5} md={6}>
            <section className="flex-column">
              <div className="title-area">
                <p>Flights</p>
              </div>
              <FlightList
                removeSelection={onRemoveFlight}
                selectedFlights={selectedFlights}
                sort={onSortSelectedFlights}
              />
            </section>
          </FlexboxGrid.Item>
        </FlexboxGrid>
      </Content>
      <Footer></Footer>
    </div>
  );
}

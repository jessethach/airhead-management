import { List, FlexboxGrid, Button } from 'rsuite';
import { Flight } from '../../../shared/models/flight';
import { useCallback } from 'react';
import { AirportLabel } from '../../../shared/components/airport-label';

export type FlightListItemProps = {
    index: number;
    flight: Flight;
    removeSelection: (index: FlightListItemProps['index']) => void;
}

export const FlightListItem = ({
    index,
    removeSelection,
    flight: { id, readable_arrival, readable_departure, destination, origin }
}: FlightListItemProps) => {
    const handleRemoveSelection = useCallback(() => {
        removeSelection(index);
    }, [removeSelection, index]);

    return (
        <List.Item index={index}>
            <section key={`row-${index}`} className="flex-column">
                <FlexboxGrid justify="space-around" key={`row-${index}`}>
                    <FlexboxGrid.Item
                        key={`col-1-in-row-${index}`}
                        colspan={6}
                        style={{
                            flexDirection: 'column',
                            alignItems: 'flex-start',
                            overflow: 'hidden'
                        }}
                    >
                    <div>Flight: {id}</div>
                    </FlexboxGrid.Item>
                    <FlexboxGrid.Item>
                        <Button onClick={handleRemoveSelection} size="xs" color="red">remove</Button>
                    </FlexboxGrid.Item>
                </FlexboxGrid>
                <FlexboxGrid justify="space-between">
                    <FlexboxGrid.Item>
                        <AirportLabel time={readable_departure} name={origin}/>
                    </FlexboxGrid.Item>
                    <FlexboxGrid.Item>
                        <AirportLabel time={readable_arrival} name={destination}/>
                    </FlexboxGrid.Item>
                </FlexboxGrid>
            </section>
        </List.Item>
    )
}

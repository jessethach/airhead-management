import { List} from 'rsuite';
import { Flight } from '../../shared/models/flight';
import { PayloadCallback } from '../../shared/types/payloadCallback';
import { FlightListItem } from './flight-list-item/flightListItem';

type FlightListProps = {
  selectedFlights: Flight[];
  removeSelection: (index: number) => void,
  sort: PayloadCallback;
}

export const FlightList = ({ selectedFlights, removeSelection, sort }: FlightListProps) => {
  return (
    <List sortable onSort={sort} hover>
      {selectedFlights && selectedFlights.map((flight, index) => (
        <FlightListItem
          key={`list-item-${index}`}
          index={index}
          flight={flight}
          removeSelection={removeSelection}
        />
      ))}
    </List>
  )
}

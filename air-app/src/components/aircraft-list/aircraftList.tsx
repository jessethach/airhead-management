import { useCallback, useEffect, useState } from 'react';
import { List, Pagination } from 'rsuite';
import { AirCraft } from '../../shared/models/aircraft';
import { useAircraftApiHook } from '../../shared/api/aircraftApiHook';
import { ResponsePagination } from '../../shared/models/genericResponse';
import { AircraftListItem } from './aircraft-list-item/aircraftListItem';

const initialData = [
  {
    ident:'GABCD',
    type:'A320',
    economySeats:186,
    base:'EGKK'
  }
];

export const AircraftList = () => {
  const [aircrafts, setAircrafts] = useState<AirCraft[]>(initialData);
  const [{ total, offset }, setPagination] = useState<ResponsePagination>({
    offset: 0,
    limit: 25,
    total: 1,
  });
  const { fetchAircrafts } = useAircraftApiHook();

  const setData = useCallback(async () => {
    try {
      const { data, pagination } = await fetchAircrafts();

      setAircrafts(data);
      setPagination(pagination);
    } catch (error) {
      console.error(error);
    }
  }, [fetchAircrafts]);

  useEffect(() => {
    setData();
  }, [setData]);
    
  return (
    <List hover>
      {aircrafts && aircrafts.map((aircraft, index) => (
        <AircraftListItem
          key={`list-item-${index}`}
          index={index}
          aircraft={aircraft}
        />
      ))}
        <Pagination
          prev
          last
          next
          first
          maxButtons={5}
          size="xs"
          pages={total}
          activePage={offset}
        />
    </List>
  )
}

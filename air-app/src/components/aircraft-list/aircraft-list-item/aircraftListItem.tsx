import { FlexboxGrid, List } from 'rsuite';
import { CSSProperties } from 'react';
import { AirCraft } from '../../../shared/models/aircraft';

const styleCenter: CSSProperties = {
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  height: '60px'
};

const titleStyle: CSSProperties = {
  paddingBottom: 5,
  whiteSpace: 'nowrap',
  fontWeight: 500
};

export type AircraftListItemProps = {
    index: number;
    aircraft: AirCraft;
}

export const AircraftListItem = ({ 
    index, aircraft: { base, economySeats }
}: AircraftListItemProps) => {
    return (
        <List.Item index={index}>
          <FlexboxGrid key={`row-${index}`}>
            <FlexboxGrid.Item
                key={`col-1-in-row-${index}`}
                colspan={6}
                style={{
                    ...styleCenter,
                    flexDirection: 'column',
                    alignItems: 'flex-start',
                    overflow: 'hidden'
                }}
            >
              <div style={titleStyle}>{base}</div>
            </FlexboxGrid.Item>
            <FlexboxGrid.Item
                key={`col-2-in-row-${index}`}
                colspan={6}
                style={{
                    ...styleCenter,
                    flexDirection: 'column',
                    alignItems: 'flex-start',
                    overflow: 'hidden'
                }}
            >
              <div style={titleStyle}>Seats: {economySeats}</div>
            </FlexboxGrid.Item>
          </FlexboxGrid>
        </List.Item>
    )
}

import { useCallback, useState, useEffect } from 'react';
import { List, Pagination } from 'rsuite';
import { Flight, FlightListItem } from '../../shared/models/flight';
import { useFlightApiHook } from '../../shared/api/flightApiHook';
import { ResponsePagination } from '../../shared/models/genericResponse';
import { RotationListItem } from './rotation-list-item/rotationListItem';

type RotationListProps = {
  selectedFlights: Flight[];
  selectFlight: (flight: Flight) => void
}

export const RotationList = ({ selectedFlights, selectFlight }: RotationListProps) => {
  const [flights, setFlights] = useState<FlightListItem[]>([]);
  const [{ total, offset }, setPagination] = useState<ResponsePagination>({
    offset: 0,
    limit: 25,
    total: 1,
  });
  const { fetchFlights } = useFlightApiHook();

  const setData = useCallback(async () => {
    try {
      const { data, pagination } = await fetchFlights();

      const flightListItems = data.map((f: Flight) => ({...f, disabled: false}))
      setFlights(flightListItems);
      setPagination(pagination);
    } catch (error) {
      console.error(error);
    }
  }, [fetchFlights]);

  useEffect(() => {
    setData();
  }, [setData]);

  useEffect(() => {
    if (selectedFlights.length < 1) {
      setFlights(prev => prev.map(f => ({...f, disabled: false})));
    } else {
      selectedFlights.forEach((sFlight) => {
        setFlights(prev => {
          return prev.map(f => {
            if (f.departuretime < sFlight.arrivaltime || f.origin !== sFlight.destination) {
              return { ...f, disabled: true }
            }
              return { ...f, disabled: false };
          })
        });
      });
    }
  }, [selectedFlights, setFlights]);
    
  return (
    <List hover>
      {flights && flights.map((flight, index) => (
        <RotationListItem
          key={`list-item-${index}`}
          index={index}
          flight={flight}
          selectFlight={selectFlight}
        />
      ))}
        <Pagination
          prev
          last
          next
          first
          maxButtons={5}
          size="xs"
          pages={total}
          activePage={offset}
        />
    </List>
  )
}

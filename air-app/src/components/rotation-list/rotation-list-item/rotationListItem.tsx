import { List, FlexboxGrid, Icon } from 'rsuite';
import { Flight, FlightListItem } from '../../../shared/models/flight';
import { useCallback } from 'react';
import { AirportLabel } from '../../../shared/components/airport-label';

export type FlightListItemProps = {
    index: number;
    flight: FlightListItem;
    selectFlight: (flight: Flight) => void
}

export const RotationListItem = ({
    index,
    selectFlight,
    flight
}: FlightListItemProps) => {
    const { id, origin, readable_departure, readable_arrival, destination, disabled } = flight;

    const handleSelection = useCallback(() => {
        if (!disabled) selectFlight(flight);
    }, [selectFlight, flight, disabled])

    return (
        <List.Item disabled={disabled} index={index} >
            <div
                className={`div-selection ${disabled ? 'disabled' : ''}` }
                style={{ backgroundColor: disabled ? '#ffcccb' : 'inherit' }}
                onClick={handleSelection}>
                <section key={`row-${index}`} className="flex-column">
                    <FlexboxGrid
                        justify="start"
                        style={{
                            alignItems: 'flex-start',
                            overflow: 'hidden',
                            paddingBottom: '2rem'
                        }}
                    >
                        <FlexboxGrid.Item>
                            <p>Flight: { id }</p>
                        </FlexboxGrid.Item>
                    </FlexboxGrid>
                    <FlexboxGrid
                        justify="space-between"
                    >
                        <FlexboxGrid.Item>
                            <AirportLabel time={readable_departure} name={origin}/>
                        </FlexboxGrid.Item>
                        <FlexboxGrid.Item>
                            <Icon icon='back-arrow' className="icon-rotate-180" size="lg" />
                        </FlexboxGrid.Item>
                        <FlexboxGrid.Item>
                            <AirportLabel time={readable_arrival} name={destination}/>
                        </FlexboxGrid.Item>
                    </FlexboxGrid>
                </section>
            </div>
        </List.Item>
    )
}

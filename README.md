### Getting Started

- Ensure you have [Node](https://nodejs.org/en/) installed. [NVM](https://github.com/nvm-sh/nvm) is a great tool for using different versions of Node if needed. Use `yarn start` to run the application on `http://localhost:3000`.

- Clone this repo

  ```
  $ git clone https://jessethach@bitbucket.org/jessethach/airhead-management.git
  ```

* Step into the `air-app` directory

  ```
  $ cd air-app
  ```

* Install dependencies

  ```
  $ yarn install
  ```

* Run the app locally

  ```
  $ yarn start
  ```

##### Tech

- React
- Rsuite

##### Env

Node v12.18.4

#### MVP

- Create application that will enable user to select an optimized flight path.

#### Stretch Goals

- Introduce routing.
- Make selected flights more dynamic. It is currently only optimizing subsequent flights based on first selection.
- Add pagination to query params.
- Concentrate more on unit and e2e testing.
